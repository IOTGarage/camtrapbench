# Camera-Trap Models Benchmarking 

Welcome to our comprehensive repository, dedicated to the world of aniaml species identification in camera trap! This repository is a treasure trove of insights, showcasing the results of seven sophisticated models designed to classify images into distinct categories: empty, animal species, vehicles, and persons.

Each model in this collection has been rigorously tested and evaluated, providing you with a detailed comparison in terms of accuracy for each class and across the different models. Our goal is to offer a transparent and comparative perspective that highlights the strengths and potential areas of improvement for each model.

## Key Features

* Seven Advanced Models: Explore the capabilities of seven cutting-edge models, each with its unique approach to identify aniamls species.

* Detailed Accuracy Comparison: Dive deep into comprehensive comparisons, showcasing the accuracy of each model per class, helping you understand the nuances of their performance.

* Interactive Model Comparison: Users are invited to bring their own models results to the table! With our user-friendly script, you can easily compare the results of your developed model against the benchmarks set by our repository's models.


# Overview

* **dataset:** Tested images for the models performance comparison.

* **ground_truth.csv:** Ground truth, where 0 for animal, 1 for human and 2 for empty.

* **ground_truth_sub_categories.json:** Ground truth, where 0 for empty, 1 for human and {2-120} for animals sup-categories. 

* **evaluation.ipynb:** This is the main code to compare all the models outputs as well to produce charts and graphs. 

* **evaluation_sub_categories.ipynb:** This is the main code to compare the models outputs ONLY for sub-categories, as well to produce charts and graphs.

* **models_weights:** Contains models for the ensemble learning (Support Vector Machine, Linear Regression and Decision Tree).

* **models_codes:** Contains codes to classify images (excluding google_cloud) through ensemble learning startegy.

* **csv:** Contains the models prediction outputs; Megadetector, Marco (Binary and Classifier), Evolving_p1 and Evolving_p2.


# Add Your model

- Open the evaluation.ipynb file.

- Modify each of: (Bold is the header in the evaluation.ipynb).

	- **Reading the csv files for tested models:** Add the path to the csv file in "custom_model" block.

	- **Add function to convert labels:** Add a function to input an image and return the class label as 0 for "animal", 1 for "human" and 2 for "empty" in the "custom_model" block.  

	- **Prepare the model's predictions:** Add an empty list for your "custom_model" and call the function to add results in the created list.

  	- **Calculate the accuracy for each model:** Add a function to calculate the accuracy as well as include your model's accuracy in the list "acc".

	- **Plot accuracies:** Add you "custom_model" name in the list "models".

	- **Custom Model:** Add a code to plot your classes.    


# Datasets

| Model Name           | Training Data                                                                                      | Testing Data                                                                                     | Notes                                                                                           |
|----------------------|----------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------|
| MegaDetector         | N/A                                                                                                | N/A                                                                                              | Authors did not provide info about the data used                                                |
| Marco & Marco_sub    | Snapshot Serengeti, Camera CATalogue, Elephant Expedition & Snapshot Wisconsin                    | Snapshot Serengeti, Camera CATalogue, Elephant Expedition & Snapshot Wisconsin                  | Same data was split for training and testing                                                    |
| Evolving 1 & 2       | Snapshot Serengeti                                                                                 | Snapshot Serengeti                                                                               | SS has various season datasets, the one used by these models has 3.2 million images.           |
| MS Species           | N/A                                                                                                | N/A                                                                                              | Authors did not provide info about the data used                                                |


## Recommended Datasets for Benckmarking

- Wildlife Insights [Click Here](https://www.wildlifeinsights.org/get-started/data-download/public#)

- Lila Science [Click Here](https://lila.science/image-access)

- Florida wildlife camera trap dataset [Click Here](https://www.crcv.ucf.edu/research/projects/florida-wildlife-camera-trap-dataset/)

- Camera trap database of Tiger from Rajaji National Park [Click Here](https://www.gbif.org/dataset/e61455a4-352d-4c55-83ea-dbca254e3b29)

- Mendeley data [Click Here](https://data.mendeley.com/datasets/6mhrhn7rxc/1)

- ENC Cairngorm [Click Here](https://www.data.gov.uk/dataset/d412d0f0-de5d-4c2a-86a8-90e2c36cf1d0/motion-activated-camera-trap-images-from-the-ecn-cairngorm-long-term-monitoring-site-2010-2022)


> **Note** that the above datasets were not used for training or testing the models used for evaluation. So, feel free to chooes any or you have the choice to bring your custom dataset. 


# Notes

- In order to compare your "custom_model" to the existing ones, you need to run the data through all the models to generate the csv files. You will find all the steps to achieve that in the following repo [Click Here](https://gitlab.com/Yaser_Cardiff/camtrapai/-/tree/master).

- The model **google_cloud** was excluded form this evaluation. If you would like to use it, please visit our repo [Click Here](https://gitlab.com/Yaser_Cardiff/camtrapai/-/tree/master) and follow the steps to set up and run the **google_cloud** model. 



