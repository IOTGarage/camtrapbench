import argparse
import ast
import pandas as pd
from joblib import load
from sklearn.metrics import accuracy_score

"""
0 = animal, 1 = human, 2 = empty
"""

def predict_and_evaluate(model_path, mega, google, marco, evolve, evolve2, output, output_file):
    # Load the trained model
    loaded_model = load(model_path)

    # Prepare the data
    X_data = []
    for i in range(len(mega)):
        X_data.append([mega[i], google[i], marco[i], evolve[i], evolve2[i]])

    # Make predictions
    predictions = loaded_model.predict(X_data)

    # Save predictions to a CSV file
    predictions_df = pd.DataFrame({'predictions': predictions})
    predictions_df.to_csv(output_file, index=False)

    # Evaluate predictions
    accuracy = accuracy_score(output, predictions)
    print("Accuracy:", accuracy)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Predict using a trained model and evaluate accuracy.")
    parser.add_argument("--model_path", help="Path to the trained model file")
    parser.add_argument("--output_file", help="Output file path to save predictions")
    parser.add_argument("--mega", required=True, help="List of 'mega' values separated by commas, e.g., '1,2,3,4,5'")
    parser.add_argument("--google", required=True, help="List of 'google' values separated by commas, e.g., '1,2,3,4,5'")
    parser.add_argument("--marco", required=True, help="List of 'marco' values separated by commas, e.g., '1,2,3,4,5'")
    parser.add_argument("--evolve", required=True, help="List of 'evolve' values separated by commas, e.g., '1,2,3,4,5'")
    parser.add_argument("--evolve2", required=True, help="List of 'evolve2' values separated by commas, e.g., '1,2,3,4,5'")
    parser.add_argument("--output", required=True, help="List of output values separated by commas, e.g., '0,1,1,2,2'")
    args = parser.parse_args()

    # Convert string representations of lists to actual lists
    mega = list(map(float, args.mega.split(',')))
    google = list(map(float, args.google.split(',')))
    marco = list(map(float, args.marco.split(',')))
    evolve = list(map(float, args.evolve.split(',')))
    evolve2 = list(map(float, args.evolve2.split(',')))
    output = list(map(int, args.output.split(',')))

    predict_and_evaluate(args.model_path, mega, google, marco, evolve, evolve2, output, args.output_file)
