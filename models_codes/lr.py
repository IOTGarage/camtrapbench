import argparse
import pandas as pd
from sklearn.metrics import accuracy_score
from joblib import load

"""
0 = animal, 1 = human, 2 = empty
"""

def predict_and_evaluate(model_path, output_file, mega, google, marco, evolve, evolve2, output):
    # Load the trained model
    loaded_model = load(model_path)

    # Prepare the data
    X_data = [[mega[i], google[i], marco[i], evolve[i], evolve2[i]] for i in range(len(mega))]

    # Make predictions
    predictions = loaded_model.predict(X_data)

    # Save predictions to a CSV file
    predictions_df = pd.DataFrame({'predictions': predictions})
    predictions_rounded = [round(pred) for pred in predictions]  # Round to the nearest integer
    predictions_df = pd.DataFrame({'predictions': predictions_rounded})
    predictions_df.to_csv(output_file, index=False)

    # Evaluate predictions
    accuracy = accuracy_score(output, predictions_rounded)
    print("Accuracy:", accuracy)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Predict using a trained model and evaluate accuracy.")
    parser.add_argument("--model_path", help="Path to the trained model file")
    parser.add_argument("--output_file", help="Output file path to save predictions")
    parser.add_argument("--mega", help="List of 'mega' values separated by commas, e.g., '1,2,3,4,5'")
    parser.add_argument("--google", help="List of 'google' values separated by commas, e.g., '1,2,3,4,5'")
    parser.add_argument("--marco", help="List of 'marco' values separated by commas, e.g., '1,2,3,4,5'")
    parser.add_argument("--evolve", help="List of 'evolve' values separated by commas, e.g., '1,2,3,4,5'")
    parser.add_argument("--evolve2", help="List of 'evolve2' values separated by commas, e.g., '1,2,3,4,5'")
    parser.add_argument("--output", help="List of output values separated by commas, e.g., '0,1,1,2,2'")
    args = parser.parse_args()

    # Convert comma-separated string representations of lists to actual lists
    mega = list(map(float, args.mega.split(','))) if args.mega else []
    google = list(map(float, args.google.split(','))) if args.google else []
    marco = list(map(float, args.marco.split(','))) if args.marco else []
    evolve = list(map(float, args.evolve.split(','))) if args.evolve else []
    evolve2 = list(map(float, args.evolve2.split(','))) if args.evolve2 else []
    output = list(map(int, args.output.split(','))) if args.output else []

    predict_and_evaluate(args.model_path, args.output_file, mega, google, marco, evolve, evolve2, output)
